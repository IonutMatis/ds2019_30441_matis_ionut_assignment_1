import React from "react";
import MyNavbar from "../navbar/MyNavbar"
import "../../../style/EntityList.css";
import NotLoggedIn from "../NotLoggedIn";
import ResourceNotFound from "../ResourceNotFound";

const MedicationPlanList = ({userModelState, medicationPlans, onDelete}) => (
    <div>
        <MyNavbar userModelState={userModelState}/>
        {
            userModelState.currentUser.role === "anonymous"
                ?
                <NotLoggedIn/>
                :
                userModelState.currentUser.role !== "doctor"
                    ?
                    <ResourceNotFound/>
                    :
                    <div>
                        <div id="table-header">
                            <h2>All medication plans</h2>
                            <button className="btn btn-success my-btn"
                                    onClick={() => window.location.assign("#/medication-plans/add")}>
                                Create
                            </button>
                        </div>
                        <table className="table table-hover">
                            <thead>
                            <tr>
                                <th scope="col">Description</th>
                                <th scope="col">Creation date</th>
                                <th scope="col">Patient</th>
                                <th scope="col">Doctor</th>
                                <th scope="col">Medications</th>
                                <th scope="col">Intake intervals</th>
                                <th scope="col">Start date</th>
                                <th scope="col">End date</th>
                                <th scope="col">Edit</th>
                                <th scope="col">Delete</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                medicationPlans.map((medicationPlan, index) => (
                                    <tr key={index}>
                                        <td>{medicationPlan.description}</td>
                                        <td>{medicationPlan.creationDate}</td>
                                        <td>{medicationPlan.patient.name}</td>
                                        <td>{medicationPlan.doctor.name}</td>
                                        <td>
                                            {
                                                medicationPlan.medications.map((medication, index2) => (
                                                    <p key={index2}>{medication.name}</p>
                                                ))
                                            }
                                        </td>

                                        <td>{medicationPlan.intakeIntervals}</td>
                                        <td>{medicationPlan.startDate}</td>
                                        <td>{medicationPlan.endDate}</td>

                                        <td>
                                            <button
                                                onClick={() => window.location.assign("#/medication-plans/edit/" + medicationPlan.id)}
                                                className="btn btn-info">
                                                Edit
                                            </button>
                                        </td>
                                        <td>
                                            <button className="btn btn-danger"
                                                    onClick={() => onDelete(medicationPlan.id)}>
                                                Delete
                                            </button>
                                        </td>
                                    </tr>
                                ))
                            }
                            </tbody>
                        </table>
                    </div>
        }
    </div>
);

export default MedicationPlanList;