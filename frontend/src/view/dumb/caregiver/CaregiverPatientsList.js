import React from "react";
import MyNavbar from "../navbar/MyNavbar"
import "../../../style/EntityList.css";
import NotLoggedIn from "../NotLoggedIn";
import ResourceNotFound from "../ResourceNotFound";

const CaregiverPatientList = ({userModelState, patients}) => (
    <div>
        <MyNavbar userModelState={userModelState}/>
        {
            userModelState.currentUser.role === "anonymous"
                ?
                <NotLoggedIn/>
                :
                userModelState.currentUser.role === "patient"
                    ?
                    <ResourceNotFound/>
                    :
                    <div>
                        <div id="table-header">
                            <h2>Patients of {userModelState.currentUser.username}</h2>
                        </div>
                        <table className="table table-hover">
                            <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Birth date</th>
                                <th scope="col">Gender</th>
                                <th scope="col">Address</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                patients.map((patient, index) => (
                                    <tr key={index}>
                                        <td>{patient.name}</td>
                                        <td>{patient.birthDate}</td>
                                        <td>{patient.gender}</td>
                                        <td>{patient.address}</td>
                                    </tr>
                                ))
                            }
                            </tbody>
                        </table>
                    </div>
        }
    </div>
);

export default CaregiverPatientList;