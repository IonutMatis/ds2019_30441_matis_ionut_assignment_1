import React from "react";
import MyNavbar from "../navbar/MyNavbar"
import "../../../style/EntityList.css";
import NotLoggedIn from "../NotLoggedIn";
import ResourceNotFound from "../ResourceNotFound";

const CaregiverList = ({userModelState, caregivers, onDelete}) => (
    <div>
        <MyNavbar userModelState={userModelState}/>
        {
            userModelState.currentUser.role === "anonymous"
                ?
                <NotLoggedIn/>
                :
                userModelState.currentUser.role !== "doctor"
                    ?
                    <ResourceNotFound/>
                    :
                    <div>
                        <div id="table-header">
                            <h2>All caregivers</h2>
                            <button className="btn btn-success my-btn"
                                    onClick={() => window.location.assign("#/caregivers/add")}>
                                Create
                            </button>
                        </div>
                        <table className="table table-hover">
                            <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Birth date</th>
                                <th scope="col">Gender</th>
                                <th scope="col">Address</th>
                                <th scope="col">Edit</th>
                                <th scope="col">Delete</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                caregivers.map((caregiver, index) => (
                                    <tr key={index}>
                                        <td>{caregiver.name}</td>
                                        <td>{caregiver.birthDate}</td>
                                        <td>{caregiver.gender}</td>
                                        <td>{caregiver.address}</td>

                                        <td>
                                            <button
                                                onClick={() => window.location.assign("#/caregivers/edit/" + caregiver.id)}
                                                className="btn btn-info">
                                                Edit
                                            </button>
                                        </td>
                                        <td>
                                            <button className="btn btn-danger"
                                                    onClick={() => onDelete(caregiver.id)}>
                                                Delete
                                            </button>
                                        </td>
                                    </tr>
                                ))
                            }
                            </tbody>
                        </table>
                    </div>
        }
    </div>
);

export default CaregiverList;