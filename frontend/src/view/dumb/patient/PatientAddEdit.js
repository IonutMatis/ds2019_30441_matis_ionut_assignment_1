import React from 'react';
import MyNavbar from "../navbar/MyNavbar";
import "../../../style/EntityList.css";
import NotLoggedIn from "../NotLoggedIn";
import ResourceNotFound from "../ResourceNotFound";

const PatientAddEdit = ({mainText, userModelState, onChange, onSubmit, patient, caregivers}) => (
    <div>
        <MyNavbar userModelState={userModelState}/>
        {
            userModelState.currentUser.role === "anonymous"
                ?
                <NotLoggedIn/>
                :
                userModelState.currentUser.role !== "doctor"
                    ?
                    <ResourceNotFound/>
                    :

                    <div id="patient-add-edit">
                        <h2>{mainText}</h2>
                        <div className="form-group row">
                            <label className="col-sm-2 col-form-label">Name</label>
                            <div className="col-sm-10">
                                <input type="text" className="form-control" placeholder="Name"
                                       onChange={e => onChange("name", e.target.value)}
                                       value={patient.name}/>
                            </div>
                        </div>

                        <div className="form-group row">
                            <label className="col-sm-2 col-form-label">Password</label>
                            <div className="col-sm-10">
                                <input type="password" className="form-control"
                                       placeholder={mainText.split(' ')[0] === "Add" ? "Password" : "Old password"}
                                       value={patient.password}
                                       onChange={e => onChange("password", e.target.value)}/>
                            </div>
                        </div>

                        <div className="form-group row">
                            <label className="col-sm-2 col-form-label">Birth date</label>
                            <div className="col-sm-10">
                                <input type="date" className="form-control" placeholder="yyyy-mm-dd"
                                       value={patient.birthDate}
                                       onChange={e => onChange("birthDate", e.target.value)}/>
                            </div>
                        </div>

                        <fieldset className="form-group">
                            <div className="row">
                                <legend className="col-form-label col-sm-2 pt-0">Gender</legend>
                                <div className="col-sm-10">
                                    <div className="form-check">
                                        <input className="form-check-input" type="radio" name="gridRadios"
                                               id="gridRadios1"
                                               value="option1"
                                               defaultChecked={patient.gender === "MALE"}
                                               onClick={() => onChange("gender", "MALE")}/>
                                        <label className="form-check-label" htmlFor="gridRadios1">
                                            Male
                                        </label>
                                    </div>
                                    <div className="form-check">
                                        <input className="form-check-input" type="radio" name="gridRadios"
                                               id="gridRadios2"
                                               value="option2"
                                               defaultChecked={patient.gender === "FEMALE"}
                                               onClick={() => onChange("gender", "FEMALE")}/>
                                        <label className="form-check-label" htmlFor="gridRadios2">
                                            Female
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <div className="form-group row">
                            <label className="col-sm-2 col-form-label">Address</label>
                            <div className="col-sm-10">
                                <input type="text" className="form-control" placeholder="Address, nr"
                                       value={patient.address}
                                       onChange={e => onChange("address", e.target.value)}/>
                            </div>
                        </div>

                        <div className="form-group row">
                            <label className="col-sm-2 col-form-label">Caregiver</label>
                            <div className="col-sm-10">
                                <select className="custom-select"
                                        onChange={e => onChange("caregiver", e.target.value)}>
                                    <option value="0">None</option>
                                    {
                                        caregivers.map((caregiver, index) => (
                                            <option key={index}
                                                    value={JSON.stringify({id: caregiver.id, name: caregiver.name})}
                                                    selected={patient.caregiver === null ? false : (patient.caregiver.name === caregiver.name)}>
                                                {caregiver.name}
                                            </option>
                                        ))
                                    }
                                </select>
                            </div>
                        </div>

                        <div className="form-group row">
                            <div className="col-sm-10">
                                <button type="submit" className="btn btn-primary my-btn"
                                        onClick={() => onSubmit()}>
                                    {mainText.split(' ')[0]}
                                </button>
                            </div>
                        </div>
                    </div>
        }
    </div>
);

export default PatientAddEdit;