import userModel from "../model/userModel";

class UserPresenter {
    onChange(property, value) {
        userModel.changeUserProperty(property, value);
    };

    onFormSubmit() {
        userModel.login()
            .then(() => {
                window.location.assign("#/" + userModel.state.currentUser.role)
            })
            .catch(() => {
                window.location.assign("#/")
            })
    }
}

const userPresenter = new UserPresenter();

export default userPresenter;