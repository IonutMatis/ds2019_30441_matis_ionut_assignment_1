export default class MedicationPlanClient {
    constructor(authorization, BASE_URL) {
        this.authorization = authorization;
        this.BASE_URL = BASE_URL;
    }

    loadMedicationPlans = () => {
        return fetch(this.BASE_URL + "/medication-plans", {
            method: "GET",
            headers: {
                "Authorization": this.authorization
            }
        }).then(response => response.json())
            .catch((err) => {
                alert("Load medicationPlans error");
            });
    };

    addMedicationPlan = (description, patient, medications, intakeIntervals, startDate, endDate) => {
        return fetch(this.BASE_URL + "/medication-plans", {
            method: "POST",
            headers: {
                "Authorization": this.authorization,
                "Content-type": "application/json"
            },
            body: JSON.stringify({
                description: description,
                patient: patient,
                medications: medications,
                intakeIntervals: intakeIntervals,
                startDate: startDate,
                endDate: endDate
            })
        }).then(response => {
            if (response.status === 200) {
                return response.json();
            } else {
                alert("Error at creating a medicationPlan");
            }
        })
    };

    editMedicationPlan(editedMedicationPlan) {
        return fetch(this.BASE_URL + "/medication-plans/edit/" + editedMedicationPlan.id, {
            method: "PUT",
            headers: {
                "Authorization": this.authorization,
                "Content-type": "application/json"
            },
            body: JSON.stringify({
                description: editedMedicationPlan.description,
                patient: editedMedicationPlan.patient,
                medications: editedMedicationPlan.medications,
                intakeIntervals: editedMedicationPlan.intakeIntervals,
                startDate: editedMedicationPlan.startDate,
                endDate: editedMedicationPlan.endDate
            })
        }).then(response => {
            if (response.status === 200) {
                return response.json();
            } else {
                alert("Error at editing a medicationPlan");
            }
        })
    }

    deleteMedicationPlan(medicationPlanId) {
        return fetch(this.BASE_URL + "/medication-plans/" + medicationPlanId, {
            method: "DELETE",
            headers: {
                "Authorization": this.authorization,
            }
        }).then(response => {
            if (response.status === 200) {
                return response;
            } else {
                alert("Error at deleting medication plan");
            }
        })
    }
}