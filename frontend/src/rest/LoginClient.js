export default class LoginClient {
    constructor(authorization, BASE_URL) {
        this.authorization = authorization;
        this.BASE_URL = BASE_URL;
    }

    login = (username, password) => {
        return fetch(this.BASE_URL + "/login", {
            method: "POST",
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
            },
            body: "username=" + username + "&password=" + password
        }).then(response => {
            if (response.status === 200) {
                return response.json();
            } else {
                throw new Error("Invalid username or password!");
            }
        })
    };
}