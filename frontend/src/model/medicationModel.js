import {EventEmitter} from "events";
import {getClientFactory} from "./userModel";

class MedicationModel extends EventEmitter {
    constructor() {
        super();
        this.state = {
            medications: [],
            newMedication: {
                name: "",
                sideEffects: "",
                dosage: ""
            },
            editedMedication: {
                id: "",
                name: "",
                sideEffects: "",
                dosage: ""
            },
        }
    }

    addMedication() {
        let newMedication = this.state.newMedication;
        let sideEffectsList = newMedication.sideEffects.trim().split(" ");
        return getClientFactory().getMedicationClient().addMedication(
            newMedication.name, sideEffectsList, newMedication.dosage
        ).then((newMedication) => {
            this.emit("changeMedication", this.state);
            return newMedication;
        })
    }

    editMedication() {
        let editedMedication = this.state.editedMedication;
        editedMedication.sideEffects = editedMedication.sideEffects.trim().split(" ");
        return getClientFactory().getMedicationClient().editMedication(editedMedication)
            .then((editedMedication) => {
                this.emit("changeMedication", this.state);
                return editedMedication;
            })
    }

    loadMedications() {
        return getClientFactory().getMedicationClient().loadMedications()
            .then(medications => {
                for (let i = 0; i < medications.length; i++) {
                    medications[i].sideEffects = medications[i].sideEffects.join(" ");
                }
                this.state = {
                    ...this.state,
                    medications: medications
                };
                this.emit("changeMedication", this.state);
            });
    }

    deleteMedication(medicationId) {
        getClientFactory().getMedicationClient().deleteMedication(medicationId)
            .then(response => {
                let remainingMedications = this.state.medications.filter(
                    medication => medication.id !== medicationId
                );
                this.state = {
                    ...this.state,
                    medications: remainingMedications
                };
                this.emit("changeMedication", this.state);
            })
    }

    changeNewMedicationProperty(property, value) {
        this.state = {
            ...this.state,
            newMedication: {
                ...this.state.newMedication,
                [property]: value
            }
        };
        this.emit("changeMedication", this.state);
    }

    changeEditedMedicationProperty(property, value) {
        this.state = {
            ...this.state,
            editedMedication: {
                ...this.state.editedMedication,
                [property]: value
            }
        };
        this.emit("changeMedication", this.state);
    }

    resetNewMedicationProperties() {
        this.state = {
            ...this.state,
            newMedication: {
                name: "",
                sideEffects: "",
                dosage: ""
            },
        };
        this.emit("changeMedication", this.state);
    }

    setEditedMedication(id) {
        let foundMedication = this.state.medications
            .filter(medication => medication.id === id);
        this.state = {
            ...this.state,
            editedMedication: foundMedication[0]
        };
        this.emit("changeMedication", this.state);
    }
}

const medicationModel = new MedicationModel();

export default medicationModel;