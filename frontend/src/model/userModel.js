import {EventEmitter} from "events";
import ClientFactory from '../rest/ClientFactory';

let clientFactory = new ClientFactory();

export function getClientFactory() {
    return clientFactory;
}

class UserModel extends EventEmitter {
    constructor() {
        super();
        this.state = {
            currentUser: {
                id: -1,
                username: "",
                password: "",
                role: "anonymous"
            },
            invalidUsernameOrPassword: null,
        }
    }

    changeUserProperty(property, value) {
        this.state = {
            ...this.state,
            currentUser: {
                ...this.state.currentUser,
                [property]: value
            }
        };
        this.emit("changeUser", this.state);
    }

    changeStateProperty(property, value) {
        this.state = {
            ...this.state,
            [property]: value
        };
        this.emit("changeUser", this.state);
    }

    login() {
        let username = this.state.currentUser.username;
        let password = this.state.currentUser.password;
        return clientFactory.getLoginClient().login(username, password)
            .then(userJson => {
                clientFactory = new ClientFactory(username, password);
                this.changeStateProperty("invalidUsernameOrPassword", false);
                this.changeUserProperty("role", userJson.role.toLowerCase());
                this.changeUserProperty("id", userJson.id);
                this.emit("changeUser", this.state);
            })
            .catch(() => {
                this.changeStateProperty("invalidUsernameOrPassword", true);
                this.emit("changeUser", this.state);
                throw new Error("Invalid username or password!");
            })
    }
}

const userModel = new UserModel();

export default userModel;