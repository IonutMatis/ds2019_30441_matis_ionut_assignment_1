import {EventEmitter} from "events";
import {getClientFactory} from "./userModel";
import patientModel from "./patientModel";

class MedicationPlanModel extends EventEmitter {
    constructor() {
        super();
        this.state = {
            medicationPlans: [],
            newMedicationPlan: {
                description: "",
                creationDate: "",
                patient: "",
                doctor: "",
                medications: [],
                intakeIntervals: "",
                startDate: "",
                endDate: "",
            },
            editedMedicationPlan: {
                id: -1,
                description: "",
                creationDate: "",
                patient: "",
                doctor: "",
                medications: [],
                intakeIntervals: "",
                startDate: "",
                endDate: "",
            }
        }
    }

    addMedicationPlan() {
        let newMedicationPlan = this.state.newMedicationPlan;
        newMedicationPlan.patient = JSON.parse(newMedicationPlan.patient);
        let newMedications = newMedicationPlan.medications.split(" ");
        let newMedicationsArray = [];
        for (let i = 0; i < newMedications.length; i++) {
            let med = {name: newMedications[i]};
            newMedicationsArray.push(med);
        }
        return getClientFactory().getMedicationPlanClient().addMedicationPlan(
            newMedicationPlan.description,
            newMedicationPlan.patient,
            newMedicationsArray,
            newMedicationPlan.intakeIntervals,
            newMedicationPlan.startDate,
            newMedicationPlan.endDate
        );
    }

    editMedicationPlan() {
        let editedMedicationPlan = this.state.editedMedicationPlan;
        editedMedicationPlan.patient = JSON.parse(editedMedicationPlan.patient);
        let newMedications = editedMedicationPlan.medications.split(" ");
        let newMedicationsArray = [];
        for (let i = 0; i < newMedications.length; i++) {
            let med = {name: newMedications[i]};
            newMedicationsArray.push(med);
        }
        editedMedicationPlan.medications = newMedicationsArray;
        return getClientFactory().getMedicationPlanClient().editMedicationPlan(editedMedicationPlan)
            .then((editedMedicationPlan) => {
                this.emit("changeMedicationPlan", this.state);
                return editedMedicationPlan;
            })
    }

    loadMedicationPlans() {
        return getClientFactory().getMedicationPlanClient().loadMedicationPlans()
            .then(medicationPlans => {
                this.state = {
                    ...this.state,
                    newMedicationPlan: {
                        ...this.state.newMedicationPlan,
                        patient: JSON.stringify(patientModel.state.patients[0])
                    },
                    medicationPlans: medicationPlans
                };
                this.emit("changeMedicationPlan", this.state);
                return medicationPlans
            });
    }

    deleteMedicationPlan(medicationPlanId) {
        getClientFactory().getMedicationPlanClient().deleteMedicationPlan(medicationPlanId)
            .then(response => {
                let remainingMedicationPlans = this.state.medicationPlans.filter(
                    medicationPlan => medicationPlan.id !== medicationPlanId
                );
                this.state = {
                    ...this.state,
                    medicationPlans: remainingMedicationPlans
                };
                this.emit("changeMedicationPlan", this.state);
            })
    }

    changeNewMedicationPlanProperty(property, value) {
        this.state = {
            ...this.state,
            newMedicationPlan: {
                ...this.state.newMedicationPlan,
                [property]: value
            }
        };
        this.emit("changeMedicationPlan", this.state);
    }

    changeEditedMedicationPlanProperty(property, value) {
        this.state = {
            ...this.state,
            editedMedicationPlan: {
                ...this.state.editedMedicationPlan,
                [property]: value
            }
        };
        this.emit("changeMedicationPlan", this.state);
    }

    resetNewMedicationPlanProperties() {
        this.state = {
            ...this.state,
            newMedicationPlan: {
                description: "",
                creationDate: "",
                patient: JSON.stringify(patientModel.state.patients[0]),
                doctor: "",
                medications: [],
                intakeIntervals: "",
                startDate: "",
                endDate: "",
            },
        };
        this.emit("changeMedicationPlan", this.state);
    }

    setEditedMedicationPlan(id) {
        let foundMedicationPlan = this.state.medicationPlans
            .filter(medicationPlan => medicationPlan.id === id);
        foundMedicationPlan[0].medications = foundMedicationPlan[0].medications
            .map(med => med.name).join(" ");
        this.state = {
            ...this.state,
            editedMedicationPlan: foundMedicationPlan[0]
        };
        this.emit("changeMedicationPlan", this.state);
    }
}

const medicationPlanModel = new MedicationPlanModel();

export default medicationPlanModel;