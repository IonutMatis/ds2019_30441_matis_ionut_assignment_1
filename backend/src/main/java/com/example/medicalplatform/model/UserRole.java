package com.example.medicalplatform.model;

import org.springframework.security.core.GrantedAuthority;

public enum UserRole {
    PATIENT,
    CAREGIVER,
    DOCTOR;
}
