package com.example.medicalplatform.dto.caregiver;

import com.example.medicalplatform.model.Caregiver;
import com.example.medicalplatform.model.Gender;
import com.example.medicalplatform.model.Patient;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CaregiverViewDTO {
    private Long id;
    private String name;
    private LocalDate birthDate;
    private String gender;
    private String address;
    private List<String> patients;

    public static Caregiver generateModelFromDTO(CaregiverViewDTO dto) {
        Caregiver caregiver = new Caregiver();
        caregiver.setId(dto.getId());
        caregiver.setName(dto.getName());
        caregiver.setBirthDate(dto.getBirthDate());
        caregiver.setGender(Gender.valueOf(dto.getGender()));
        caregiver.setAddress(dto.getAddress());

        return caregiver;
    }

    public static CaregiverViewDTO generateDTOFromModel(Caregiver caregiver) {
        CaregiverViewDTO dto = new CaregiverViewDTO();
        dto.setId(caregiver.getId());
        dto.setName(caregiver.getName());
        dto.setBirthDate(caregiver.getBirthDate());
        dto.setGender(caregiver.getGender().name());
        dto.setAddress(caregiver.getAddress());
        dto.setPatients(caregiver.getPatients().stream()
                .map(Patient::getName)
                .collect(Collectors.toList()));

        return dto;
    }
}
