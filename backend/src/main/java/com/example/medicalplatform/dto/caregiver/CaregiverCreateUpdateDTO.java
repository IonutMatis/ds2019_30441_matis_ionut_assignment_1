package com.example.medicalplatform.dto.caregiver;

import com.example.medicalplatform.model.Caregiver;
import com.example.medicalplatform.model.Gender;
import com.example.medicalplatform.model.UserRole;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.ArrayList;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CaregiverCreateUpdateDTO {
    private String name;
    private String password;
    private LocalDate birthDate;
    private String gender;
    private String address;

    public static Caregiver generateModelFromDTO(CaregiverCreateUpdateDTO dto) {
        Caregiver caregiver = new Caregiver();
        caregiver.setName(dto.getName());
        caregiver.setPassword(dto.getPassword());
        caregiver.setRole(UserRole.CAREGIVER);
        if (dto.getBirthDate() != null) {
            caregiver.setBirthDate(dto.getBirthDate());
        } else {
            caregiver.setBirthDate(LocalDate.now());
        }
        caregiver.setGender(Gender.valueOf(dto.getGender()));
        caregiver.setAddress(dto.getAddress());
        caregiver.setPatients(new ArrayList<>());

        return caregiver;
    }
}
