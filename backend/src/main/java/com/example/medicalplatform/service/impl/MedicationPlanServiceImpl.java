package com.example.medicalplatform.service.impl;

import com.example.medicalplatform.dto.medicationPlan.MedicationPlanDTO;
import com.example.medicalplatform.exception.UnknownMedicationException;
import com.example.medicalplatform.exception.UnknownMedicationPlanException;
import com.example.medicalplatform.exception.UnknownPatientException;
import com.example.medicalplatform.model.*;
import com.example.medicalplatform.repository.DoctorRepository;
import com.example.medicalplatform.repository.MedicationPlanRepository;
import com.example.medicalplatform.repository.MedicationRepository;
import com.example.medicalplatform.repository.PatientRepository;
import com.example.medicalplatform.service.MedicationPlanService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional
public class MedicationPlanServiceImpl implements MedicationPlanService {
    private final MedicationPlanRepository medicationPlanRepository;
    private final PatientRepository patientRepository;
    private final DoctorRepository doctorRepository;
    private final MedicationRepository medicationRepository;

    @Override
    public MedicationPlanDTO saveUpdate(MedicationPlanDTO medicationPlanDTO, Long id) {
        MedicationPlan medicationPlan = MedicationPlanDTO.generateModelFromDTO(medicationPlanDTO);

        Patient patient = patientRepository.findById(medicationPlanDTO.getPatient().getId())
                .orElseThrow(UnknownPatientException::new);

        MyUserDetails currentUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Doctor doctor = (Doctor) currentUserDetails.getUser();
        List<Medication> medications = medicationPlanDTO.getMedications().stream()
                .map(simpleMedicationDTO -> medicationRepository.findByName(simpleMedicationDTO.getName())
                        .orElseThrow(UnknownMedicationException::new))
                .collect(Collectors.toList());

        medicationPlan.setPatient(patient);
        medicationPlan.setDoctor(doctor);

        medicationPlan.getMedications().addAll(medications);

        if (id != null) {
            medicationPlan.setId(id);
        }
        MedicationPlan savedMedicationPlan = medicationPlanRepository.save(medicationPlan);
        medicationRepository.saveAll(medications);

        return MedicationPlanDTO.generateDTOFromModel(savedMedicationPlan);
    }

    @Override
    public MedicationPlanDTO findById(Long id) {
        MedicationPlan foundMedicationPlan = medicationPlanRepository.findById(id).orElseThrow(UnknownMedicationPlanException::new);

        return MedicationPlanDTO.generateDTOFromModel(foundMedicationPlan);
    }

    @Override
    public List<MedicationPlanDTO> findAll() {
        return medicationPlanRepository.findAll().stream()
                .map(MedicationPlanDTO::generateDTOFromModel)
                .collect(Collectors.toList());
    }

    @Override
    public void deleteById(Long id) {
        MedicationPlan currentMedicationPlan = medicationPlanRepository.findById(id)
                .orElseThrow(UnknownMedicationPlanException::new);
        currentMedicationPlan.getPatient().getMedicalRecord().remove(currentMedicationPlan);
        currentMedicationPlan.setMedications(null);
        currentMedicationPlan.setDoctor(null);
        currentMedicationPlan.setPatient(null);
        medicationPlanRepository.deleteById(id);
    }
}
