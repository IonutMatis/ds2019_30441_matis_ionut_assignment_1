package com.example.medicalplatform.service;

import com.example.medicalplatform.dto.SimpleUserDTO;
import com.example.medicalplatform.dto.caregiver.CaregiverCreateUpdateDTO;
import com.example.medicalplatform.dto.caregiver.CaregiverViewDTO;
import com.example.medicalplatform.dto.patient.PatientSimpleViewDTO;
import com.example.medicalplatform.model.Patient;

import java.util.List;

public interface CaregiverService {
    CaregiverViewDTO saveUpdate(CaregiverCreateUpdateDTO caregiverCreateUpdateDTO, Long id);

    CaregiverViewDTO findById(Long id);

    List<CaregiverViewDTO> findAll();

    void deleteById(Long id);

    List<PatientSimpleViewDTO> findPatientsByCaregiverId(Long caregiverId);

    CaregiverViewDTO addPatientToCaregiver(SimpleUserDTO<Patient> patient, Long caregiverId);
}
