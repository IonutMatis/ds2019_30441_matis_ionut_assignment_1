package com.example.medicalplatform.controller;

import com.example.medicalplatform.dto.medicationPlan.MedicationPlanDTO;
import com.example.medicalplatform.service.MedicationPlanService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/medication-plans")
public class MedicationPlanController {
    private final MedicationPlanService medicationPlanService;

    @GetMapping(value = "/{id}")
    public MedicationPlanDTO findById(@PathVariable("id") Long id) {
        return medicationPlanService.findById(id);
    }

    @GetMapping
    public List<MedicationPlanDTO> findAll() {
        return medicationPlanService.findAll();
    }

    @PostMapping
    public MedicationPlanDTO save(@RequestBody MedicationPlanDTO medicationPlanDTO) {
        return medicationPlanService.saveUpdate(medicationPlanDTO, null);
    }

    @PutMapping("/edit/{id}")
    public MedicationPlanDTO update(@RequestBody MedicationPlanDTO patientCreateUpdateDTO,
                                    @PathVariable("id") Long id) {
        return medicationPlanService.saveUpdate(patientCreateUpdateDTO, id);
    }

    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable("id") Long id) {
        medicationPlanService.deleteById(id);
    }
}
