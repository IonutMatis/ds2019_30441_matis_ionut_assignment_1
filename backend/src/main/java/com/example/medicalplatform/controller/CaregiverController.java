package com.example.medicalplatform.controller;

import com.example.medicalplatform.dto.SimpleUserDTO;
import com.example.medicalplatform.dto.caregiver.CaregiverCreateUpdateDTO;
import com.example.medicalplatform.dto.caregiver.CaregiverViewDTO;
import com.example.medicalplatform.dto.patient.PatientSimpleViewDTO;
import com.example.medicalplatform.model.Patient;
import com.example.medicalplatform.service.CaregiverService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/caregivers")
public class CaregiverController {
    private final CaregiverService caregiverService;

    @GetMapping(value = "/{id}")
    public CaregiverViewDTO findById(@PathVariable("id") Long id) {
        return caregiverService.findById(id);
    }

    @GetMapping
    public List<CaregiverViewDTO> findAll() {
        return caregiverService.findAll();
    }

    @PostMapping
    public CaregiverViewDTO save(@RequestBody CaregiverCreateUpdateDTO caregiverCreateUpdateDTO) {
        return caregiverService.saveUpdate(caregiverCreateUpdateDTO, null);
    }

    @PutMapping("/edit/{id}")
    public CaregiverViewDTO update(@RequestBody CaregiverCreateUpdateDTO caregiverCreateUpdateDTO,
                                       @PathVariable("id") Long id) {
        return caregiverService.saveUpdate(caregiverCreateUpdateDTO, id);
    }

    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable("id") Long id) {
        caregiverService.deleteById(id);
    }

    @GetMapping(value = "/{id}/patients")
    public List<PatientSimpleViewDTO> getPatientsForCaregiver(@PathVariable("id") Long caregiverId) {
        return caregiverService.findPatientsByCaregiverId(caregiverId);
    }

    @PostMapping(value = "/{id}/patients")
    public CaregiverViewDTO addPatientToCaregiver(@PathVariable("id") Long caregiverId,
                                                  @RequestBody SimpleUserDTO<Patient> patient) {
        return caregiverService.addPatientToCaregiver(patient, caregiverId);
    }
}
