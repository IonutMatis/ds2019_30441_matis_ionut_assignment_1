package com.example.medicalplatform.repository;

import com.example.medicalplatform.model.Doctor;
import com.example.medicalplatform.model.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DoctorRepository extends JpaRepository<Doctor, Long> {
    @Query("SELECT d from Doctor d WHERE d.id = :id")
    Optional<Doctor> findById(@Param("id") Long id);

    @Query("from Doctor")
    List<Doctor> findAll();

    @Modifying
    @Query("DELETE from Doctor where id = :id")
    void deleteById(@Param("id") Long id);

    Optional<Doctor> findByName(String name);
}
