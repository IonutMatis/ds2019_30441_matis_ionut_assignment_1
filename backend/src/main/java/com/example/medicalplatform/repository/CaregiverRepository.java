package com.example.medicalplatform.repository;

import com.example.medicalplatform.model.Caregiver;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CaregiverRepository extends JpaRepository<Caregiver, Long> {
    @Query("SELECT c from Caregiver c where c.id = :id")
    Optional<Caregiver> findById(@Param("id") Long id);

    @Query("from Caregiver")
    List<Caregiver> findAll();

    @Modifying
    @Query("DELETE from Caregiver WHERE id = :id")
    void deleteById(@Param("id") Long id);
}
