package com.example.medicalplatform.repository;

import com.example.medicalplatform.model.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PatientRepository extends JpaRepository<Patient, Long> {
    @Query("SELECT p from Patient p WHERE p.id = :id")
    Optional<Patient> findById(@Param("id") Long id);

    @Query("from Patient")
    List<Patient> findAll();

    @Modifying
    @Query("DELETE from Patient where id = :id")
    void deleteById(@Param("id") Long id);

    @Query(value = "SELECT * FROM USERS WHERE caregiver_id = ?1", nativeQuery = true)
    List<Patient> findByCaregiverId(Long caregiverId);
}
